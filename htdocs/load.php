<a href="http://envato.com/" id="picture" target="_blank">
	<img src="http://www.nratraffic.ie/Camera%20Images/N8lGreenhills_snap.jpg"></img>
</a>
<h2>Latest Traffic News</h2>
<p>A crash inbound on St. John's Rd West just before Heuston Station is blocking one lane and causing delays. Busy then on the North Quays up to Bachelors Walk.
  In the city centre, a bus has broken down on St. Stephen's Green North near the jct with Kildare St. Take care on approach.
  Traffic lights are out of action at the jct of Dundrum Rd and Bird Ave. Take care on approach.
  No major delays to report for M50 traffic in either direction. </p>