
// Author: Stephen McCarthy

/* // Wait for PhoneGap to load */
//

document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is loaded and it is now safe to make calls PhoneGap methods
//
function onDeviceReady() {

    checkConnection(); //Call the function to check for internet connection when the device is ready
}//end onDeviceReady

//Check for the state of the internet connection
function checkConnection() {

    //Use phonegap api to check
    var networkState = navigator.network.connection.type;

    //Create an empty variable
    var states = {};

    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.NONE] = 'No network connection';

    //Debug to see if working
    //alert('Connection type: ' + states[networkState]);




    //If the there is no internet connection on start up give the user a warning
    if (networkState == Connection.NONE) {
        //show the UIAlertView
        alert('Connection type: ' + states[networkState] + 'Please connect to the internet');

    }//end if


}//end check connection





/////***************************** SIMULATE LOAD EXTERNAL HTML FROM A SERVER ************************////////////////////


//example of loading external HTML from a server                           
var ajax_load = "<img class='loading' src='../images/load.gif' alt='loading...' />";

//SET THE PHP FILE TO A VARIABLE
var loadUrl = "../htdocs/load.php";

//CALLED I AJAX.GET
$(document).on('pageshow', function () {


    //console.log("EXT HTML working");//testing
    //Load external HTML
    $("#loadHTMLButton").on('click', function () {

        $("#result").html(ajax_load).load(loadUrl);

    }); //END CLICK
}); //END PAGEINIT


//FUNCTION CALLED IN loadPartDOM.html

//LOAD ONLY PART OF THE DOM ON THE SERVER - IE THE IMAGE
$(document).ready("#load_dom").click(function () {
    //console.log("working");
    $("#pictureResult").html(ajax_load).load(loadUrl + " #picture");
});


//FUNCTION CALLED IN loadHTMLIntoDOM.html	                
$(document).ready("#load_get").click(function () {
    $("#resultGET").html(ajax_load);
    $.get(
    loadUrl, {
        language: "php",
        version: 5
    },

    function (responseText) {
        $("#resultGET").html(responseText);
    },
        "html");
});


////////************************* END EXTERNAL HTML ************************************//////////////////////







////////////************************* START PUBLIC FLICKR CALL *********************///////////////////////////


//FUNCTION GETTING CALLED IN LOADPUBLICFLICKR.HTML
$(document).on('pageshow', function () {

    $("#loadPublicFlickr").on('click', function () {
    
	    $('#loading').append("Contacting Flickr.......... </br>");//tell the user what is happening
        //console.log("Public Flickr working");//TESTING

        var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
       
        $.getJSON(flickerAPI, {
            tags: "iphone",
            tagmode: "any",
            format: "json"
        })
            .done(function (data) {
             $('#loading').empty();//empty the loading div when the data has loaded on the page
            $.each(data.items, function (i, item) {
            

                $("<img/>").attr("src", item.media.m).appendTo("#resultPublicFlickr");

                if (i === 10) {
                
                    //End when the images loaded reaches ten    
                    return false;
                   
                } //END IF
            }); //END EACH
        }); //END DONE
    }); //END CLICK
}); //END PAGESHOW                


////////*********************************** END FLICKR PUBLIC CALL ***********************///////////////////////








///////******************* PHOTOSET FLICKR QUERY *****************************/////////////////


//Load the flickr content in view views/flickrCalls.html


$(document).on('pageshow', function () {

    $('#flickrAjaxButton').on("click", function () {

        //console.log("Flickr Photoset working"); testing   
        
        $('#loadingPhotoset').append("Contacting Flickr..........");//tell the user what is happening          


        var url = 'http://api.flickr.com/services/rest/?&method=flickr.photosets.getList&api_key=e6be1bfdf4294482904dce01ac431615&user_id=56454781@N00&format=json&jsoncallback=?';

        //GET THE JSON FROM THE URL AND PASS IT INTO THE FUNCTION
        $.getJSON(url, function (data) {
        $('#loadingPhotoset').empty();//empty the loading div when the data has loaded on the page
        $('#gallery').empty();
            //ITERATE THROUGH THE JSON
            $.each(data.photosets.photoset, function (idx, item) {

                var photosetid = item.id;



                //BUILD THE URL FOR THE PHOTOSET
                var photosurl = 'http://api.flickr.com/services/rest/?&method=flickr.photosets.getPhotos&api_key=e6be1bfdf4294482904dce01ac431615&photoset_id=' + photosetid + '&media=photos&format=json&jsoncallback=?&per_page=1';


                //PARSE THE JSON IN(TITLE, DESCRIPTION, PHOTO, COUNT_VIEWS, COUNT_COMMENTS) AND APPEND IT TO DIV GALLERY

                $('#gallery').append('<h3>' + item.title._content + '</h3> <h5>' + item.description._content +
                    '</h5> <p>Number Of Views: ' + item.count_views + ' <p>Number Of Photos: ' + item.photos + ' <p>Number Of Comments: ' + item.count_comments + ' <ul class="photos grp' + idx + '"></ul>');
                    

                //HALT THE ITERATION AFTER THE COUNT OF PHOTOS REACHES 10
                if (idx === 10) {
                    return false;
                }

                //BUILD THE PHOTO URL FROM THE JSON
                $.getJSON(photosurl, function (data) {
                    $.each(data.photoset.photo, function (photoNum, photo) {
                        var photoid = photo.id;
                        var secret = photo.secret;
                        var server = photo.server;
                        var farm = photo.farm;

                        var photo_url = 'http://farm' + farm + '.static.flickr.com/' + server + '/' + photoid + '_' + secret + '_s.jpg';
                        var photo_urlm = 'http://farm' + farm + '.static.flickr.com/' + server + '/' + photoid + '_' + secret + '_m.jpg';
                        $('.photos.grp' + idx).append('<li><a href="' + photo_urlm + '"><img src="' + photo_url + '" /></a></li>');
                    });
                });
            });
        }); //END GET JSON


    }); //END CLICK FUNCTION

}); //END PAGE INIT



/////****************************** END FLICKR PHOTOSETS AJAX CALL **************************/////////////////////







///////////////**************** TWITTER CALL TO GET TRAFFIC NEWS FROM AAROADWATCH TIMELINE**************////////////

//CALLED IN TRAFFICNEWS.HTML

//SET THE JSON FEED TO A VARIABLE
var twitterAPI = "https://api.twitter.com/1/statuses/user_timeline/aaroadwatch.json?count=10&include_rts=1&callback=?";

$(document).on('pageshow', function () {

    //console.log("AA ROAD"); //TESTING

    //BUTTON CLICK
    $("#loadTwitterTraffic").on('click', function () {
	    $("#loadingTraff").append("Contacting Twitter.......");
        $("#twitter").empty(); //empty the div on refresh click

        $.getJSON(twitterAPI, function (data) {
        $("#loadingTraff").empty();
            //FOR EACH .TEXT FOUND IN THE JSON APPEND IT TO THE #TWITTER DIV
            $.each(data, function (i, result) {
	    	            
                //src="'+result.user.profile_image_url+'" alt="" height="42" width="42" />');

                $("#twitter").append('<p> Following Posted On: "' + result.created_at + '"</p>');
                $("#twitter").append('<p> "' + result.text + '"</p>');
                $("#twitter").append('</div>');

            }); //END EACH 
        }); //END GETJSON
    }); //END CLICK       
}); //END PAGESHOW


/////////////////////////////////////********************* END TWITTER CALL ****************************////////////////////




///////////////**************** TWITTER CALL TO GET WEATHER NEWS TIMELINE**************////////////

//CALLED IN POPUPPANEL IN INDEX.HTML

//SET THE JSON FEED TO A VARIABLE
var weatherAPI = "https://api.twitter.com/1/statuses/user_timeline/IrelandsWeather.json?count=10&include_rts=1&callback=?";

$(document).on('pageshow', function () {

    //console.log("WEATHER TWITTER"); //TESTING

    //BUTTON CLICK
    $("#loadWeather").on('click', function () {

        $("#weather").empty(); //empty the div


        $.getJSON(weatherAPI, function (data) {
            //FOR EACH .TEXT FOUND IN THE JSON APPEND IT TO THE #TWITTER DIV
            $.each(data, function (i, result) {

                //src="'+result.user.profile_image_url+'" alt="" height="42" width="42" />');

                //$("#twitter").append('<p> @ "'+result.user.name+'"</p>');
                $("#weather").append('<p> "' + result.text + '"</p>');
                $("#weather").append('</div>');
                if (i === 3) {

                    return false;
                }

            }); //END EACH 
        }); //END GETJSON
    }); //END CLICK       
}); //END PAGESHOW


/////////////////////////////////////********************* END WEATHER TWITTER CALL ****************************////////////////////








/////////****************************************** MAKE AN AJAX CALL TO TWITTER & PULL DOWN USER PROFILE INFO **************////////////////////


$(document).on('pageshow', function () {

    $("#btnLookup").on('click', function () {

        //console.log("Twitter working"); testing

        //set the variable for the username from the user input in getFollowers.html
        var tUser = $('#tUsername').val();

        $.ajax({
            url: 'http://api.twitter.com/1/users/lookup.json?screen_name=' + tUser,
            type: 'GET',
            dataType: 'jsonp',
            timeout: 5000,
            beforeSend: function () {
                $('#t-container').append("Contacting Twitter...");
            },

            //ON SUCCESS DO THIS
            success: function (data) {
                //SET THE HTML INTO A VARIABLE TO APPEND INTO A DIV
                var htmlTwitter = '<div class="t-card">' +
                    '<img class="t-img" src="' + data[0].profile_image_url + '" />' +
                    '<label class="t-name">' + data[0].name + ' </label>' +
                    '(<a class="t-username" href="http://twitter.com/' + data[0].screen_name + '">@' + data[0].screen_name + '</a>)<br/>' +
                    '<label class="t-loc">' + data[0].location + '</label>' +
                    '<p class="t-desc">' + data[0].description + '</p>' +
                    '<a class="t-href" href="' + data[0].url + '">' + data[0].url + '</a><br/>' +
                    '<ul class="t-stats">' +
                    '<li>Tweets<br /><span class="t-count">' + data[0].statuses_count + '</span></li>' +
                    '<li>Following<br /><span class="t-count">' + data[0].friends_count + '</span></li>' +
                    '<li>Followers<br /><span class="t-count">' + data[0].followers_count + '</span></li>' +

                    '<li>Listed On<br /><span class="t-count">' + data[0].listed_count + '</span></li>' +
                    '<li>Account Created On<br /><span class="t-count">' + data[0].created_at + '</span></li>' +

                    '</ul>' +
                    '</div>';

                $('#t-container').html(htmlTwitter); //LOAD THE RESULTS INTO THE DIV
            }, //END BEFORE SEND
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Unable to find the Twitter contact. Please try a different username.");
            } //END ERROR

        }); //END AJAX    

    }); //END CLICK
}); //END PAGESHOW 

///************************ END TWITTER AJAX CALL ****************************//////////////////







////////******************* SET THE HEIGHT OF THE POP OUT PANEL *****************/////////////////////

//CALLED IN INDEX.HTML

//Because the popup container is positioned absolute, you can't make the panel full height with height:100%;. 
//This small script sets the height of the popup to the actual screen height.
//Taken from jquery API
$(document).on('pageinit', function () {
    $("#popupPanel").on({
        popupbeforeposition: function () {
            var h = $(window).height();

            $("#popupPanel").css("height", h);
        }
    }); //END POPUPPANEL
}); //END PAGEINIT

///////////////**************** END POP OUT PANEL ****************************////////////////////////








///////////////////***************** AJAX CALL TO FACEBOOK GRAPH ***************/////////////////////////

//CALLED IN VIEWS.FACEBOOKGRAPH.HTML

//Set the variable for the facebook API
var facebookAPI = "http://graph.facebook.com/140918675956744/";

$(document).on('pageshow', function () {

    //console.log("facebook");
    $("#facebookButton").on('click', function () {
    

        //empty the div so it doesn't keep repeating
        $("#profile").empty();
        
        $.getJSON(facebookAPI, function (data) {
               
            //ADD THE DATA TO THE PROFILE DIV IN FACEBOOKGRAPH.HTML
            $("#profile").append("<h2> Name: " + data.name + "</h2>");
            $("#profile").append("<h4> About Us: " + data.about + "</h4>");
            $("#profile").append("<h4> Category: " + data.category + "</h4>");
            $("#profile").append("<p> Founded: " + data.founded + "</p>");
            $("#profile").append("<p> Number of Likes: " + data.likes + "</p>");
            $("#profile").append("<p> Link: " + data.link + "</p>");
            $("#profile").append("<p> Mission: " + data.mission + "</p>");
            $("#profile").append("<p> Talked about " + data.talking_about_count + " times</p>");
            
            
        }); //End JSON return	
    }); //End click handler
}); //End pageshow


////////////////*************** END FACEBOOK GRAPH CALL ********************/////////////////////////////









//////////********************* LOAD DATA USING A CROSS-DOMAIN AJAX CALL *****************//////////////////




//SET A VARIABLE TO OUR RSS FEED
var site = "http://rss.nratraffic.ie/rsstraveltimes.xml";

// Take the provided url, and add it to a YQL query and encode it
// NOTES: SELF; THE YAHOO API ACTS AS AN INTERMEDIARY FOR THE CALL, ALMOST LIKE A PROXY
var yql = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent('select channel.item.description from xml where url="' + site + '"') + '&format=xml&callback=?';

//FUNCTION GETTING FIRED IN views/news.html    
$(document).on('pageshow', function () {

    //console.log("RSS working");//testing    

    $("#loadXML").on('click', function () {

        // Request that YSQL string, and run a callback function.
        $.getJSON(yql, getTrafficNews);

        function getTrafficNews(data) {
            // If we have something to work with...
            if (data.results[0]) {

                //WRITE THE RESULT TO THE #LOADTRAFFIC DIV NEWS.HTML                         
                document.getElementById("loadTraffic").innerHTML = (data.results[0]);
                //document.write(data.results[0]);  
                //alert(data.results[0]);

            } //END IF

            // Else, Maybe we requested a site that doesn't exist, and nothing returned.
            else alert('Failed to load, please try again later');
        } //END ELSE
    }); //EMD CLICK 
}); //END PAGESHOW

////////////////////************** END CROSS-DOMAIN CALL ************************//////////////////////////







///////////////**************** LOAD GOOGLE MAPS *****************************/////////////////////////

//CALLED IN MAPS.HTML

function initialize() {

    //console.log("MAP LOAD");//TESTING
    //SET THE LAT AND LONG THE MAP LOADS AT	
    var centreDublin = new google.maps.LatLng(53.348147, -6.2677),
        markers,
        //CONFIGURE THE OPTIONS ON START UP
        myMapOptions = {
            zoom: 14, /// ZOOM LEVEL ON LOAD
            center: centreDublin,
            mapTypeId: google.maps.MapTypeId.ROADMAP //SWITCH ROADMAP FOR ANOTHER OPTION 
        },

        //SET THE MAP TO DISPLAY IN THE MAP_CANVAS
        map = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


    // INITIATE THE MAP MARKERS INTO AN ARRAY    
    function initMarkers(map, markerData) {
        var newMarkers = [],
            marker;

        //SET THE LOOP TO THE LENGTH OF THE ARRAY    
        for (var i = 0; i < markerData.length; i++) {
            //CONFIGURE THE OPTIONS
            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: markerData[i].latLng,
                visible: true
            }),
            //CREATE AN ELEMENT FOR THE BOX
            boxText = document.createElement("div"),
            //these are the options for all infoboxes
            infoboxOptions = {
                content: boxText,
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(-140, 0),
                zIndex: null,
                boxStyle: {
                    background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                    opacity: 0.75,
                    width: "120px" //SHORTEN THE WIDTH
                },
                closeBoxMargin: "8px 4px 2px 2px",
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                infoBoxClearance: new google.maps.Size(1, 1),
                isHidden: false,
                pane: "floatPane",
                enableEventPropagation: false
            };

            newMarkers.push(marker);
            //define the text and style for all infoboxes
            boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background:#333; color:#FFF; font-family:Arial; font-size:12px; padding: 5px; border-radius:6px; -webkit-border-radius:6px; -moz-border-radius:6px;";
            boxText.innerHTML = markerData[i].address + "<br>" + markerData[i].state;
            //Define the infobox
            newMarkers[i].infobox = new InfoBox(infoboxOptions);
            //Open box when page is loaded
            newMarkers[i].infobox.open(map, marker);
            //Add event listen, so infobox for marker is opened when user clicks on it.  Notice the return inside the anonymous function - this creates
            //a closure, thereby saving the state of the loop variable i for the new marker.  If we did not return the value from the inner function, 
            //the variable i in the anonymous function would always refer to the last i used, i.e., the last infobox. This pattern (or something that
            //serves the same purpose) is often needed when setting function callbacks inside a for-loop.
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    newMarkers[i].infobox.open(map, this);
                    map.panTo(markerData[i].latLng);
                }
            })(marker, i));
        }

        return newMarkers;
    }

    //here the call to initMarkers() is made with the necessary data for each marker.  All markers are then returned as an array into the markers variable
    markers = initMarkers(map, [{
        latLng: new google.maps.LatLng(53.347116, -6.258801),
        address: "O'Connell Street",
        state: ""
    }, //oconnell bridge
    {
        latLng: new google.maps.LatLng(53.343166, -6.244762),
        address: "Pearse Street",
        state: "South"
    }, //pearse street
    {
        latLng: new google.maps.LatLng(53.34834, -6.255378),
        address: "South Quays",
        state: ""
    } //south quays
    ]);


}



/////FUNCTION TO GET THE CURRENT POSITION OF THE USER
//w3schools eg

//set the div to a variable X
var x = document.getElementById("demo");

function getLocation() {
    if (navigator.geolocation) {
        //console.log("call location");
        navigator.geolocation.getCurrentPosition(showPosition);
    }//end if
    //alert("Location cannot be found. Please try again later.");
    else {
        x.innerHTML = "Location cannot be found. Please try again later.";
    }//end if else
}//end getLocation

function showPosition(position, showPosition) {
    //console.log("append pos");

    $('#demo').append(" Loading GPS.........");
    //APPEND THE LAT-LONG TO THE DIV

    document.getElementById("demo").innerHTML = ("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
    //x.innerHTML="Latitude: " + position.coords.latitude + 
    //"<br>Longitude: " + position.coords.longitude;	

}//end showPosition

/////////////*************************** END GOOGLE MAP FUNCTION ********************//////////////////////////////////////